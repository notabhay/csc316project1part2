package edu.ncsu.csc316.log.manager;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.ncsu.csc316.log.factory.DSAFactory;

/**
 * Tests
 *
 * @author Abhay Sriwastawa
 */
public class LogReportManagerTest {

    /** Field for Manager */
	LogReportManager manager;

	/**
	 * Setup
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		new DSAFactory();
		manager = new LogReportManager("input/activityLog_sample.csv");
	}

    /**
     * Test
     */
	@Test
	public void testTopUserActivities() {
		assertEquals(
				"Top User Activities Report [\n" + "   13: sort HL7 Code 422\n" + "   2: print office visit OV02132\n"
						+ "   1: unmerge notification NX1115\n" + "   1: view HL7 Code 422\n" + "]",
				manager.getTopUserActivitiesReport(4));

		manager = new LogReportManager("input/inputCustom1.csv");
		assertEquals("The log file does not contain any user activities", manager.getTopUserActivitiesReport(1));

		assertEquals("Please enter a number > 0", manager.getTopUserActivitiesReport(0));

		manager = new LogReportManager("input/inputCustom2.csv");
		assertEquals(
				"Top User Activities Report [\n" + "   13: sort HL7 Code 422\n" + "   2: print office visit OV02132\n"
						+ "   2: view HL7 Code 422\n" + "   1: unmerge notification NX1115\n" + "]",
				manager.getTopUserActivitiesReport(4));

		manager = new LogReportManager("input/inputCustom3.csv");
		assertEquals("Top User Activities Report [\n" + "   1: print office visit OV02132\n"
				+ "   1: sort HL7 Code 422\n" + "]", manager.getTopUserActivitiesReport(4));
	}

    /**
     * Test
     */
	@Test
	public void testEntriesByDate() {
		assertEquals(
				"Activities recorded on 11/13/2015 [\n" + "   gphsu, 11/13/2015 06:36:48AM, sort, HL7 Code 422\n" + "]",
				manager.getDateReport("11/13/2015"));

		manager = new LogReportManager("input/inputCustom1.csv");
		assertEquals("No activities were recorded on 01/01/2000", manager.getDateReport("01/01/2000"));

		manager = new LogReportManager("input/inputCustom2.csv");
		assertEquals(
				"Activities recorded on 11/13/2015 [\n" + "   gphsu, 11/13/2015 06:36:48AM, sort, HL7 Code 422\n" + "]",
				manager.getDateReport("11/13/2015"));

		assertEquals("Please enter a valid date in the format MM/DD/YYYY", manager.getDateReport("1/01/2000"));
		assertEquals("Please enter a valid date in the format MM/DD/YYYY", manager.getDateReport("01/32/2000"));
		assertEquals("No activities were recorded on 01/29/2000", manager.getDateReport("01/29/2000"));
		assertEquals("No activities were recorded on 01/31/2000", manager.getDateReport("01/31/2000"));
	}

    /**
     * Test
     */
	@Test
	public void testEntriesByHour() {
		assertEquals(
				"Activities recorded during hour 6 [\n" + "   gphsu, 11/13/2015 06:36:48AM, sort, HL7 Code 422\n" + "]",
				manager.getHourReport(6));

		manager = new LogReportManager("input/inputCustom1.csv");
		assertEquals("No activities were recorded during hour 1", manager.getHourReport(1));

		manager = new LogReportManager("input/inputCustom2.csv");
		assertEquals(
				"Activities recorded during hour 6 [\n" + "   gphsu, 11/13/2015 06:36:48AM, sort, HL7 Code 422\n" + "]",
				manager.getHourReport(6));

		assertEquals("Please enter a valid hour between 0 (12AM) and 23 (11PM)", manager.getHourReport(25));
		assertEquals("Please enter a valid hour between 0 (12AM) and 23 (11PM)", manager.getHourReport(-1));
	}
}
