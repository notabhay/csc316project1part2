/**
 * @file CSV_Generator
 * @author Trevor Leibert
 *
 * Generates a CSV file with random data for CSC316 Project 1
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

// whether to to seed the random generator with the current time
// if set to false the same log file will be generated every time
#define USE_RANDOM_SEED true

#define MAX_USERNAME_LENGTH 10
#define MIN_USERNAME_LENGTH 4

// this can be set between 0 and 9998, must be lower than YEAR_UPPER_BOUND
#define YEAR_LOWER_BOUND 1970
// this can be set between 1 and 9999, must be higher than YEAR_LOWER_BOUND
#define YEAR_UPPER_BOUND 2050

// defs for generating random usernames
#define NUM_LETTERS 26
#define LOWERCASE_A 97

// generated dates won't have days past 28 to avoid having to check for febuary
#define MIN_DAYS_IN_MONTH 28

// list of possible actions to choose from
#define ACTIONS {"print", "sort", "unmerge", "view"}
#define NUM_ACTIONS 4

// list of possible resources to choose from
#define RESOURCES {"office visit OV02132", "HL7 Code 422", "notification NX1115"}
#define NUM_RESOURCES 3

void generateUsername(int length, char username[MAX_USERNAME_LENGTH + 1]) {
    for (int i = 0; i < length; i++) {
        username[i] = rand() % NUM_LETTERS + LOWERCASE_A;
    }

    username[length] = '\0';
}

int main(int argc, char const *argv[])
{
    if (argc != 3) {
        printf("Usage: CSV_Generator FILENAME NUM_ENTRIES\n");
        exit(1);
    }

    FILE *f = fopen(argv[1], "w");
    const int numEntries = atoi(argv[2]);
    const char am[] = "AM";
    const char pm[] = "PM";
    const char *actions[NUM_ACTIONS] = ACTIONS;
    const char *resources[NUM_RESOURCES] = RESOURCES;

    if (USE_RANDOM_SEED) {
        srand(time(0));
    }
    fprintf(f, "USERNAME, TIMESTAMP, ACTION, RESOURCE\n");
    for (int i = 0; i < numEntries; i++) {
        char username[MAX_USERNAME_LENGTH + 1];
        int length = rand() % (MAX_USERNAME_LENGTH - MIN_USERNAME_LENGTH) + MIN_USERNAME_LENGTH + 1;
        generateUsername(length, username);

        int month = rand() % 12 + 1;
        int day = rand() % MIN_DAYS_IN_MONTH + 1;
        int year = rand() % (YEAR_UPPER_BOUND - YEAR_LOWER_BOUND) + YEAR_LOWER_BOUND + 1;
        int hour = rand() % 12 + 1;
        int minute = rand() % 60;
        int second = rand() % 60;
        int choice = rand() % NUM_ACTIONS;
        int code = rand() % NUM_RESOURCES;
        bool isAm = rand() & 1;

        fprintf(f, "%s, %02d/%02d/%04d %02d:%02d:%02d%2s, %s, %s\n", username, month,
            day, year, hour, minute, second, isAm ? am : pm, actions[choice], resources[code]);
    }


    fclose(f);
    return 0;
}
