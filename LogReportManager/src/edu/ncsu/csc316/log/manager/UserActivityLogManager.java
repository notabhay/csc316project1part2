package edu.ncsu.csc316.log.manager;

import edu.ncsu.csc316.dsa.list.List;
import edu.ncsu.csc316.dsa.map.Map;
import edu.ncsu.csc316.dsa.sorter.Sorter;
import edu.ncsu.csc316.log.data.LogEntry;
import edu.ncsu.csc316.log.factory.DSAFactory;
import edu.ncsu.csc316.log.io.LogEntryReader;

/**
 * This manager class processes a list of log entries to facilitate analytics,
 * including: - identifying the most frequently performed user activities -
 * grouping log entries by date - grouping log entries by hour of the day
 *
 * @author Dr. King
 * @author Abhay Sriwastawa
 *
 */
public class UserActivityLogManager {

	private List<LogEntry> list;

	/**
	 * Constructs a new manager to process the log entries contained in the file at
	 * the specified path
	 *
	 * @param pathToFile the path to the input log entry file
	 */
	public UserActivityLogManager(String pathToFile) {
		list = LogEntryReader.loadLogEntries(pathToFile);
	}

	/**
	 * Returns a List of the most frequently performed N user activities in the
	 * input log entry file. Returns an empty List if the log contains no entries.
	 *
	 * @param number the number of user activities to include in the frequency
	 *               report
	 * @return a List of the most frequently performed N user activities in the
	 *         input log entry file
	 */
	public List<String> getTopActivities(int number) {
		// Array to Return
		List<String> returnArray = DSAFactory.getIndexedList(); // empty array of size n
		if (list.isEmpty()) {
			return returnArray;
		}
		Sorter<String> stringSort = DSAFactory.getComparisonSorter();
		String[] sortedArray = new String[list.size()];
		StringBuilder actionPlusResource = new StringBuilder("");
		for (int i = 0; i < list.size(); i++) {
			actionPlusResource = new StringBuilder("");
			actionPlusResource.append(list.get(i).getAction());
			actionPlusResource.append(" ");
			actionPlusResource.append(list.get(i).getResource());
			sortedArray[i] = actionPlusResource.toString();
		}
		stringSort.sort(sortedArray);
		ActionFrequency[] actionFrequencyList = new ActionFrequency[list.size()];
		int actionCounter = 1;
		ActionFrequency actionWithFrequency = new ActionFrequency(sortedArray[actionCounter - 1], 1);
		actionFrequencyList[actionCounter - 1] = actionWithFrequency;
		for (int i = 1; i < sortedArray.length; i++) {
			if (sortedArray[i].equals(sortedArray[i - 1])) {
				actionWithFrequency.incrementFrequency();
			} else {
				actionCounter++;
				actionWithFrequency = new ActionFrequency(sortedArray[i], 1);
				actionFrequencyList[actionCounter - 1] = actionWithFrequency;
			}
		}
        ActionFrequency[] actionFrequencyListShort = new ActionFrequency[actionCounter];
        for (int i = 0; i < actionCounter; i++) {
            actionFrequencyListShort[i] = actionFrequencyList[i];
        }
		Sorter<ActionFrequency> actionFrequencySort = DSAFactory.getComparisonSorter();
		actionFrequencySort.sort(actionFrequencyListShort);
		for (int i = 0; i < number && i < actionCounter; i++) {
			returnArray.add(i, actionFrequencyListShort[i].toString());
		}
		return returnArray;
	}

    /**
     * Private Inner Class to Implement ActionFrequency
     *
     * @author Abhay Sriwastawa
     */
	private class ActionFrequency implements Comparable<ActionFrequency> {
        /** Private Field */
		private String action;
        /** Private Field */
		private int frequency;

        /**
         * Constructor
         *
         * @param action    action
         * @param frequency frequency
         */
		ActionFrequency(String action, int frequency) {
			this.action = action;
			this.frequency = frequency;
		}

		@Override
		public int compareTo(ActionFrequency o) {
			if (frequency < o.getFrequency()) {
				return 1;
			} else if (frequency > o.getFrequency()) {
				return -1;
			} else {
				return this.action.compareTo(o.getAction());
			}
		}

        /**
         * Get Frequency
         *
         * @return frequency
         */
		public int getFrequency() {
			return this.frequency;
		}

        /**
         * Get Action
         *
         * @return Action
         */
        public String getAction() {
			return this.action;
		}

        /**
         * Increment Frequency by 1
         */
		public void incrementFrequency() {
			frequency++;
		}

        /**
         * Convert ActionFrequency to string
         *
         * @return string
         */
		public String toString() {
			StringBuilder returnString = new StringBuilder();
			returnString.append(frequency);
			returnString.append(": ");
			returnString.append(action);
			return returnString.toString();
		}
	}

	/**
	 * Returns a Map that represents the List of log entries performed on each
	 * unique date. For the Map, the String key represents the date in the format
	 * MM/DD/YYYY For the Map, the List (value of entry) represents the list of log
	 * entries performed on that date. Returns an empty Map if the log contains no
	 * entries.
	 *
	 * @return a Map that represents the List of log entries performed on each
	 *         unique date
	 */
	public Map<String, List<LogEntry>> getEntriesByDate() {
		// Empty Map to return
		Map<String, List<LogEntry>> returnMap = DSAFactory.getMap();

		if (list.isEmpty()) {
			return returnMap;
		}

		// Sorter
		Sorter<LogEntry> sort = DSAFactory.getComparisonSorter();

		// Sort list
		LogEntry[] tempArray = new LogEntry[list.size()];
		for (int i = 0; i < list.size(); i++) {
			tempArray[i] = list.get(i);
		}
		sort.sort(tempArray);
		for (int i = 0; i < list.size(); i++) {
			list.set(i, tempArray[i]);
		}

		String key = list.get(0).getTimestamp().toString().substring(0, 10);
		List<LogEntry> value = DSAFactory.getIndexedList();
		value.add(0, list.get(0));
		returnMap.put(key, value);

		for (int i = 1; i < list.size(); i++) {
			if (!list.get(i).getTimestamp().toString().substring(0, 10).equals(key)) {
				key = list.get(i).getTimestamp().toString().substring(0, 10);
				value = DSAFactory.getIndexedList();
				value.addLast(list.get(i));
				returnMap.put(key, value);
			} else {
				returnMap.get(key).addLast(list.get(i));
			}
		}
		return returnMap;
	}

	/**
	 * Returns a Map that represents the List of log entries performed during each
	 * hour of the day. For the Map, the Integer key represents the hour of the day
	 * (from 0-23, where 0=12AM-1AM; 1 = 1AM-2AM; etc.) in the format MM/DD/YYYY For
	 * the Map, the List (value of entry) represents the list of log entries
	 * performed during the given hour of the day. Returns an empty Map if the log
	 * contains no entries.
	 *
	 * @return a Map that represents the List of log entries performed during each
	 *         hour of the day
	 */
	public Map<Integer, List<LogEntry>> getEntriesByHour() {
		// Empty Map to return
		Map<Integer, List<LogEntry>> returnMap = DSAFactory.getMap();

		if (list.isEmpty()) {
			return returnMap;
		}

		// Sorter
		Sorter<LogEntry> sort = DSAFactory.getComparisonSorter();

		// Sort list
		LogEntry[] tempArray = new LogEntry[list.size()];
		for (int i = 0; i < list.size(); i++) {
			tempArray[i] = list.get(i);
		}
		sort.sort(tempArray);
		for (int i = 0; i < list.size(); i++) {
			list.set(i, tempArray[i]);
		}

		List<LogEntry> value = DSAFactory.getIndexedList();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getTimestamp().isAM()) {
				if (returnMap.get((list.get(i).getTimestamp().getHour() + 12) % 12) == null) {
					value.addLast(list.get(i));
					returnMap.put((list.get(i).getTimestamp().getHour() + 12) % 12, value);
				} else {
					returnMap.get((list.get(i).getTimestamp().getHour() + 12) % 12).addLast(list.get(i));
				}
			} else {
				if (returnMap.get(((list.get(i).getTimestamp().getHour() + 12) % 12) + 12) == null) {
					value.addLast(list.get(i));
					returnMap.put(((list.get(i).getTimestamp().getHour() + 12) % 12) + 12, value);
				} else {
					returnMap.get(((list.get(i).getTimestamp().getHour() + 12) % 12) + 12).addLast(list.get(i));
				}
			}
			value = DSAFactory.getIndexedList();
		}

		return returnMap;
	}
}
