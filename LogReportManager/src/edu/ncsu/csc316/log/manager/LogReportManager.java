package edu.ncsu.csc316.log.manager;

import edu.ncsu.csc316.dsa.list.List;
import edu.ncsu.csc316.dsa.map.Map;
import edu.ncsu.csc316.log.data.LogEntry;

/**
 * LogReportManager interacts to retrieve log file analytics and format the
 * output for the user interface
 *
 * @author Dr. King
 * @author Abhay Sriwastawa
 *
 */
public class LogReportManager {

	private UserActivityLogManager activityLogManager;

	/**
	 * Constructs a new manager to process the log file at the given path
	 *
	 * @param pathToFile the path to the input file with log entries
	 */
	public LogReportManager(String pathToFile) {
		activityLogManager = new UserActivityLogManager(pathToFile);
	}

	/**
	 * Returns a report of the N most frequently performed user activities in the
	 * input log file.
	 *
	 * @param number the number of user activities to include in the report
	 * @return a report of the N most frequently performed user activities
	 */
	public String getTopUserActivitiesReport(int number) {
		if (number < 1) {
			return "Please enter a number > 0";
		}
		List<String> topActivities = activityLogManager.getTopActivities(number);
		if (topActivities.isEmpty()) {
			return "The log file does not contain any user activities";
		}
		StringBuilder returnString = new StringBuilder("Top User Activities Report [\n");
		for (int i = 0; i < topActivities.size(); i++) {
			returnString.append("   ");
			returnString.append(topActivities.get(i));
			returnString.append("\n");
		}
		returnString.append("]");
		return returnString.toString();
	}

	/**
	 * Returns a report of log entries that were recorded on a specified date
	 *
	 * @param date the date for which to retrieve log entries
	 * @return a report of log entries that were recorded on the specified date
	 */
	public String getDateReport(String date) {
		if (!date.matches("([0-1]{1}[1-9]{1})/([0-2]{1}[0-9]{1})/([0-9]{4})")
				&& !date.matches("([0-1]{1}[1-9]{1})/([3]{1}[0-1]{1})/([0-9]{4})")) {
			return "Please enter a valid date in the format MM/DD/YYYY";
		}
		Map<String, List<LogEntry>> map = activityLogManager.getEntriesByDate();
		List<LogEntry> logEntryList = map.get(date);
		if (logEntryList == null) {
			return "No activities were recorded on " + date;
		}
		StringBuilder returnString = new StringBuilder("Activities recorded on " + date + " [\n");
		for (int i = 0; i < logEntryList.size(); i++) {
			LogEntry temp = logEntryList.get(i);
			returnString.append("   ");
			returnString.append(temp.getUsername());
			returnString.append(", ");
			returnString.append(temp.getTimestamp().toString());
			returnString.append(", ");
			returnString.append(temp.getAction());
			returnString.append(", ");
			returnString.append(temp.getResource());
			returnString.append("\n");
		}
		returnString.append("]");
		return returnString.toString();
	}

	/**
	 * Returns a report of log entries that were recorded during the specified hour
	 * of the day, where 0 = 12AM-1AM; 1 = 1AM-2AM; 2 = 2AM-3AM; ... 23 =
	 * 11PM-midnight
	 *
	 * @param hour the hour for which to retrieve log entries
	 * @return a report of log entries that were recorded during the specified hour
	 *         of the day
	 */
	public String getHourReport(int hour) {
		if (hour < 0 || hour > 23) {
			return "Please enter a valid hour between 0 (12AM) and 23 (11PM)";
		}
		Map<Integer, List<LogEntry>> map = activityLogManager.getEntriesByHour();
		List<LogEntry> logEntryList = map.get(hour);
		if (logEntryList == null) {
			return "No activities were recorded during hour " + hour;
		}
		StringBuilder returnString = new StringBuilder("Activities recorded during hour " + hour + " [\n");
		for (int i = 0; i < logEntryList.size(); i++) {
			LogEntry temp = logEntryList.get(i);
			returnString.append("   ");
			returnString.append(temp.getUsername());
			returnString.append(", ");
			returnString.append(temp.getTimestamp().toString());
			returnString.append(", ");
			returnString.append(temp.getAction());
			returnString.append(", ");
			returnString.append(temp.getResource());
			returnString.append("\n");
		}
		returnString.append("]");
		return returnString.toString();
	}
}
